Koha Documentation
==================

This is the main repository for the Koha manual.   
It is mirrored at git.koha-community.org.

The manual is generated with Sphinx.

Setup
-----

> sudo apt-get install make python3-sphinx python3-sphinxcontrib.spelling python3-pip
>
> sudo pip3 install sphinx_rtd_theme

If you have python 3.11 or more, the second command might give you a warning message and fail.

To find your python version

> python3 -V

Instead of the command to install sphinx_rtd_theme described above, do the following

> python3 -m venv env #create virtual environment
>
> source env/bin/activate #enter virtual environment
>
> pip install sphinx_rtd_theme #install sphinx_rtd_theme

Outputing Docs
--------------

If using python 3.11 or more, you need to enter your virtual environment first

> source env/bin/activate

### For HTML
> make html

### For HTML in a single page
>  make singlehtml

### For an epub
>  make epub

### For a pdf (Work in progress)

Install latexmk, the latexpdf builder needs it

> apt install latexmk 

Install texlive-full. Attention: this is very huge

> apt install texlive-full 

Generate the latex files

> make latex 

Generate the PDF file

> make latexpdf


Other useful commands
---------------------

To find everything that you can do, you can just do

> make

And it will list all the available options. Some useful ones are

### Check spelling
> make spelling

### Check links in the documentation
> make linkcheck

### Generate .po files for translation

Have a look at the [koha-manual-i18n](https://gitlab.com/koha-community/koha-manual-i18n) project.
